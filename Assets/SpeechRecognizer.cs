﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Networking;
using System;
using System.IO;
using System.Collections;

//TESTSETSETSET
//edit from web!!!
//mnorer more edit!!
//test conflict/sdfsdfsdf
//sdfsdf

enum SpeechState
{
    Talking, Idle
}

[System.Serializable]
public class MSResponse
{
    public string RecognitionStatus;
    public string DisplayText;
    public int Offset;
    public int Duration;
}

[System.Serializable]
public class WitResponse
{
    public string _text;
    public string entities;
    public string msg_id;    
}

[System.Serializable]
public class Alternative
{
    public float confidence;
    public string transcript;
}

[System.Serializable]
public class Result {
    public Alternative[] alternatives;
}

[System.Serializable]
public class IBMResponse
{
    public Result[] results;
}

public class SpeechRecognizer : MonoBehaviour {
    public Text outputMS;
    public Text outputWit;
    public Text outputIBM;
    public Text outputStatus;

    AudioClip clip;
    int micTimeout = 10;
    int sampleRate = 16000;
    int avgSeconds = 1;
    float threshold = 0.01f;
    SpeechState speechState = SpeechState.Idle;
    int startPos;
    
    void Start () {
        clip = Microphone.Start("Built-in Microphone", true, micTimeout, sampleRate);
    }
	
	void Update () {
        if (clip != null)
        {
            float sum = 0.0f;
            float[] data = new float[sampleRate * avgSeconds];

            int currentPos = Microphone.GetPosition(null);
            if (currentPos - sampleRate * avgSeconds < 0)
                clip.GetData(data, clip.samples + currentPos - sampleRate * avgSeconds);
            else
                clip.GetData(data, currentPos - sampleRate * avgSeconds);

            for (int i = 0; i < sampleRate * avgSeconds; ++i)
            {
                sum += Mathf.Abs(data[i]);
            }
            sum /= sampleRate * avgSeconds;

            outputStatus.text = string.Format("AvgVolume = {0}\nstate = {1}", sum, speechState);
            if (sum > threshold && speechState == SpeechState.Idle)
            {
                speechState = SpeechState.Talking;

                startPos = Microphone.GetPosition(null) - sampleRate / 2;
                if (startPos < 0)
                    startPos += clip.samples;
            }
            if (sum < threshold && speechState == SpeechState.Talking)
            {
                speechState = SpeechState.Idle;

                int lastPos = Microphone.GetPosition(null);

                if (lastPos - startPos < 0)
                    data = new float[clip.samples - startPos + lastPos + 1];
                else
                    data = new float[lastPos - startPos + 1];
                clip.GetData(data, startPos);
                
                AudioClip talkClip = AudioClip.Create("tempClip", data.Length, 1, 16000, false, null);
                talkClip.SetData(data, 0);

                string filepath;
                byte[] wavFile = WavUtility.FromAudioClip(talkClip, out filepath, false);

                StartCoroutine(MSConvertAudio(wavFile));
                StartCoroutine(WitConvertAudio(wavFile));
                StartCoroutine(IBMConvertAudio(wavFile));
            }
        }
    }

    IEnumerator IBMConvertAudio(byte[] input)
    {
        var downloader = new DownloadHandlerBuffer();

        var uploader = new UploadHandlerRaw(input);

        UnityWebRequest request = new UnityWebRequest("https://8d39d951-b64e-43c6-9fd4-85d759023b82:2JlA4rxwKMlT@stream.watsonplatform.net/speech-to-text/api/v1/recognize",
            UnityWebRequest.kHttpVerbPOST);
        request.SetRequestHeader("Content-type", "audio/wav");
        request.downloadHandler = downloader;
        request.uploadHandler = uploader;

        yield return request.SendWebRequest();

        string jsonText = System.Text.Encoding.UTF8.GetString(downloader.data);
        IBMResponse response = JsonUtility.FromJson<IBMResponse>(jsonText);

        outputIBM.text = string.Format("[IBM]: {0}", response.results[0].alternatives[0].transcript);
    }

    IEnumerator WitConvertAudio(byte[] input)
    {
        var downloader = new DownloadHandlerBuffer();

        var uploader = new UploadHandlerRaw(input);

        UnityWebRequest request = new UnityWebRequest("https://api.wit.ai/speech",
            UnityWebRequest.kHttpVerbPOST);
        request.SetRequestHeader("Authorization", "Bearer UVAC464G5732NVC6API466IZ6EGRRX2J");
        request.SetRequestHeader("Content-type", "audio/wav");
        request.downloadHandler = downloader;
        request.uploadHandler = uploader;

        yield return request.SendWebRequest();
        

        string jsonText = System.Text.Encoding.UTF8.GetString(downloader.data);
        WitResponse response = JsonUtility.FromJson<WitResponse>(jsonText);

        outputWit.text = string.Format("[Wit]: {0}", response._text);
    }

    IEnumerator MSConvertAudio(byte[] input)
    {
        var downloader = new DownloadHandlerBuffer();

        var uploader = new UploadHandlerRaw(input);

        UnityWebRequest request = new UnityWebRequest("https://speech.platform.bing.com/speech/recognition/interactive/cognitiveservices/v1?language=en-US&format=simple",
            UnityWebRequest.kHttpVerbPOST);
        request.SetRequestHeader("Ocp-Apim-Subscription-Key", "14d8676cb82c417a9050df4bf6136f84");
        request.SetRequestHeader("Content-type", "audio/wav; codec=audio/pcm; samplerate=16000");
        request.downloadHandler = downloader;
        request.uploadHandler = uploader;

        yield return request.SendWebRequest();
        
        string jsonText = System.Text.Encoding.UTF8.GetString(downloader.data);
        MSResponse response = JsonUtility.FromJson<MSResponse>(jsonText);

        outputMS.text = string.Format("[MS]: {0}", response.DisplayText);
    }
}
